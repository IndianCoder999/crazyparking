﻿using CrazyParkingServer.Messages;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System.Text;

namespace CrazyParkingServer.Controllers
{
    public class NotificationController : Controller
    {
        readonly IPublishEndpoint _publishEndpoint;

        public NotificationController(IPublishEndpoint publishEndpoint)
        {
            _publishEndpoint = publishEndpoint;
        }

        [HttpPost]
        [Route("/message")]
        public async Task<IActionResult> SendMessage(string apiKey, string chatId, string message)
        {
            var notification = new TelegramNotification() { ApiKey = apiKey, ChatId = chatId, Message = message };
            await _publishEndpoint.Publish<TelegramNotification>(notification) ;
            return Ok();
        }
    }
}
