﻿using System;
using System.Collections.Generic;

namespace CrazyParkingServer.Models
{
    public partial class Item
    {
        public Item()
        {
            Orders = new HashSet<Order>();
        }

        public int ItemId { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }
        public decimal? Cost { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
