﻿namespace CrazyParkingServer.Models
{
    public interface IUserRepository
    {
        public IQueryable<User> users { get; }
    }
}
