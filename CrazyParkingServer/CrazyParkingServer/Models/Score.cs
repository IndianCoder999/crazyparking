﻿using System;
using System.Collections.Generic;

namespace CrazyParkingServer.Models
{
    public partial class Score
    {
        public int ScoreId { get; set; }
        public DateTime Timestamp { get; set; }
        public int UserId { get; set; }
        public int Score1 { get; set; }

        public virtual User User { get; set; } = null!;
    }
}
