﻿namespace CrazyParkingServer.Models
{
    public class UserRepository:IUserRepository
    {
        private GameDbContext _gameDbContext;
        public UserRepository(GameDbContext gameDbContext)
        {
            _gameDbContext = gameDbContext;
        }
        public IQueryable<User> users => _gameDbContext.Users;

    }
}
