﻿namespace NotificationServer.Services
{
    public interface ITelegramService:IService
    {
        public void Send(string apiKey, string chatId, string message);
    }
}
