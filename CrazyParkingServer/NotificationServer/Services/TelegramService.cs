﻿using Telegram.Bot;

namespace NotificationServer.Services
{
    public class TelegramService : ITelegramService
    {
        public async void Send(string apiKey, string chatId, string message)
        {
            //apikey = 5658363288:AAHxBMPxYtsA2fWTnHfE1Rio0Cqx2uhjtrQ
            //chatid = -1001788171039
            var bot = new TelegramBotClient(apiKey);
            await bot.SendTextMessageAsync(chatId, message);
        }
    }
}
