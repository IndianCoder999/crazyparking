﻿namespace CrazyParkingServer.Messages
{
    public class TelegramNotification
    {
        public string ApiKey { get; set; }
        public string ChatId { get; set; }
        public string Message { get; set; }
    }
}
