﻿using CrazyParkingServer.Messages;
using MassTransit;
using NotificationServer.Services;

namespace NotificationServer.Consumers
{
    public class NotificationConsumer : IConsumer<TelegramNotification>
    {
        readonly ILogger _logger;
        readonly ITelegramService _telegramService;
        public NotificationConsumer(ITelegramService telegramService, ILogger<NotificationConsumer> logger)
        {
            _telegramService = telegramService;
            _logger = logger;
        }
        public Task Consume(ConsumeContext<TelegramNotification> context)
        {
            _logger.LogInformation("Notification received: {message}", context.Message.Message);
            _telegramService.Send(context.Message.ApiKey, context.Message.ChatId, context.Message.Message);
            return Task.CompletedTask;
        }
    }

    public class NotificationConsumerDefinition : ConsumerDefinition<NotificationConsumer>
    {
        public NotificationConsumerDefinition()
        {
            EndpointName = "notification-service";
            ConcurrentMessageLimit = 8;
        }

        protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<NotificationConsumer> consumerConfigurator)
        {
            endpointConfigurator.UseMessageRetry(r => r.Intervals(100, 200, 500, 800, 1000));
            endpointConfigurator.UseInMemoryOutbox();
        }
    }
}
